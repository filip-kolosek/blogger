Rails.application.routes.draw do
  resources :articles do
    resources :comments
  end
  # resources :articles only: [:index, :show]
  root to: 'articles#index'
  resources :tags
end
